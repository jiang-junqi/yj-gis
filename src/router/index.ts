import { RouteRecordRaw, createRouter, createWebHistory } from 'vue-router'
import Layout from '@/views/Layout/index.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Layout',
    component: Layout
  }
]

export const router = createRouter({
  history: createWebHistory(),
  routes
})