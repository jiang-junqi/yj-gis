import { createApp } from 'vue'
import './style.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import '@/styles/element/index.scss'
import { router } from './router'
import pinia from './store'

const app = createApp(App)

app
  .use(ElementPlus)
  .use(router)
  .use(pinia)
  .mount('#app')
